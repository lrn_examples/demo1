package com.my.servlets.pkg;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/MyServlet")
public class MyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// not getAttribute() - it will be null (nobody set it yet)
		// String myInputTxt = (String) request.getAttribute("myInput");
		String myInputTxt = request.getParameter("myInput");

		request.setAttribute("myAttrName", myInputTxt);

		request.getSession().setAttribute("myAttrName", "session_MyText1");
		
		
		// application.setAttribute("myAttrName", "application_MyText1");
		// application in JSP is ServletContext in Servlet
		ServletContext servletContext = request.getServletContext();
		servletContext.setAttribute("myAttrName", "applicationScope_MyText2");
		
		request.getSession().getServletContext().setAttribute("myAttrName", "session_MyText2");
		
		
		// leading slash is a MUST
		// shall be WAR-root relative aka docroot/context-relative
		// (WebContent maps to WAR root - see Deployment Assembly).
		// request.getRequestDispatcher is request-relative (if no / used, otherwise
		// same!):
		// https://stackoverflow.com/questions/1411114/servletcontext-getrequestdispatcher-vs-servletrequest-getrequestdispatcher/1411532
		RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher("/myresponse.jsp");
		requestDispatcher.forward(request, response);

//		response.getWriter().append("Served at: ").append(request.getContextPath());
//		response.getWriter().append(myInputTxt);
		// response.getWriter().flush();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
