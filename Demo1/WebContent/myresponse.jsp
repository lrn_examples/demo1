<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>



<body>

<%@ page import="java.io.IOException" %>

    <% application.setAttribute("myAttrName", "application_MyText1"); %>

	<!-- html comment CANNOT COMMENT-OUT SCRIPTLET! USE SCRIPTLET-COMMENT  -->
	<!-- scriptlet below would override "myAttrName" set in Servlet! --!>
	<!-- would override ${requestScope} as well, it sets requestScope.attr! -->
	<%-- 
         <% request.setAttribute("myAttrName", "myValue"); %>
    --%>
    
    <!-- both don't interfere with anything  below -->  
    <% request.getSession().setAttribute("myAttrName", "session_MyText1"); %>
    <!-- same as application.setAttribute() -->
    <% request.getSession().getServletContext().setAttribute("myAttrName", "request_getSession()_getServletContext()_setAttribute"); %>
    <% session.setAttribute("myAttrName", "session_MyText1"); %>
   
    
    <!-- getSessionContext() is deprecated in! -->
    <!-- Already missing in Tomcat9 servlet-api.jar --> 
    <%--
       <% request.getSession().getSessionContext().setAttribute("myAttrName", "session_MyText"); %>
     --%>   
     
    
   

	<!-- html attributes don't 
           interfere with EL (request, requestScope) / request.getAttribute() -->
	<div class="myAttrName"></div>

	<!-- overrides only ${myAttrName} -->
	<%-- 
	    <% pageContext.setAttribute("myAttrName", "pageContext_MyText"); %>
    --%>

	<br>
	<br>

	<!-- default EL scope: EL will search the page, request, 
	session and application scopes in sequence 
	for the first non-null attribute value matching 
	the attribute name) -->

	EL Way (default EL scope - \${pageScope.myAttrName}): ${myAttrName}
	<!-- EL expression -->

	<br>
	<br>

	<!-- in case you have <h attr="myAttrName"> on this JSP page -->
	EL Way (EL-requestScope - \${requestScope.myAttrName}):
	${requestScope.myAttrName}

	<br>
	<br>

	<!-- scriptlet way -->
	<%
		String str1 = (String) request.getAttribute("myAttrName");
	%>

	Scriptlet way1 (request.getAttribute()):
	<%=str1%>

	<br>
	<br>

	<!-- <input name="myInput"/> ACCESSIBLE AFTER FORWARDING! -->
	<%
		String str2 = request.getParameter("myInput");
	%>

	Scriptlet way2 (request.getParameter()): <%=str2%>

	<br>
	<br>
	
	<!-- I didn't set it so that ${myAttrName} sees ${requestScope.myAttrName} -->
	pageScope: ${pageScope.myAttrName}
	<br><br>
	requestScope: ${requestScope.myAttrName}
	<br><br>
	sessionScope: ${sessionScope.myAttrName}
	<br><br>
	applicationScope: ${applicationScope.myAttrName}
	
	




</body>
</html>