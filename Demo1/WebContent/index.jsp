<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="font-size:14px;">

<form action="MyServlet" method="get">

		<input name="myInput" title="enter text" /> <br>
		<br> <input type="submit" value="Submit" />

	</form>

<br>
	<pre >
1) Form input from the form below (this JSP) is sent into Servlet.

2)   Servlet then reads input, 
2.1) sets it as attribute,
2.2) forwards request to JSP.

3) JSP then reads the attribute with EL and scriptlets,

and reads input (parameters) with scriptlets.

4) Demo of these Implicit Objects:

	pageContext (JSP only)

	request (JSP scriptlet / Servlet)
	
	session (JSP scriptlet / request.getSession() in Servlet)
	
	application (JSP scriptlet / request.getServletContext() in Servlet)
	
	+ EL:   \${pageScope.attr1}, 
			\${requestScope.attr1}, 
			\${sessionScope.attr1},
			\${applicationScope.attr1}
  
</pre>
	<br>
	<br>

	

</body>
</html>